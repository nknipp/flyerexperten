module.exports = {
  "env": {
    "browser": false,
    "commonjs": true,
    "es6": true,
    "node": true,
    "jest": true,
    "mongo": true
  },
  "extends": "eslint:recommended",
  "parserOptions": {
    "ecmaFeatures": {
      "experimentalObjectRestSpread": true,
      "jsx": true
    }
  },
  "plugins": [
    "async-await"
  ],
  "rules": {
    "indent": [
      "error",
      2,
      {"SwitchCase": 1}
    ],
    "linebreak-style": [
      "error",
      "unix"
    ],
    "quotes": [
      "error",
      "single"
    ],
    "semi": [
      "error",
      "always"
    ],
    "no-console": 0,
    "async-await/space-after-async": 2,
    "async-await/space-after-await": 2
  }
};