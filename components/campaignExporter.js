const Excel = require('exceljs');
const moment = require('moment');
const { MongoClient } = require('mongodb');
const CronJob = require('cron').CronJob;
const cryptoUtils = require('../utils/crypto');
const mailer = require('../utils/mailer');
const connection = require('./databaseConnection');

function export2excel(campaignObjs) {
  const workbook = new Excel.stream.xlsx.WorkbookWriter();
  const worksheet = workbook.addWorksheet('Gewinnspielteilnehmer');
  worksheet.columns = [
    { header: 'Lösung', key: 'solution', width: 40 },
    { header: 'Vorname', key: 'firstname', width: 30 },
    { header: 'Nachname', key: 'lastname', width: 30 },
    { header: 'Straße', key: 'street', width: 30 },
    { header: 'Postleitzahl', key: 'postcode', width: 10 },
    { header: 'Ort', key: 'city', width: 30 },
    { header: 'E-Mail', key: 'email', width: 30 },
    { header: 'Abgabedatum', key: 'createdAt', width: 20 }
  ];
  for (const campaignObj of campaignObjs) {
    worksheet.addRow(campaignObj).commit();
  }
  return workbook;
}

async function getCampaigns(db) {
  const today = moment().hours(0).minutes(0).seconds(0);
  const yesterday = moment(today).subtract(1, 'days');
  return await db.collection('campaigns').find({
    startDate: {$lt: today.toDate()},
    closingDate: {$gte: yesterday.toDate()} // send last report tomorrow
  }).toArray();
}

async function exportCampaigns() {
  const db = await connection.connectDB();
  try {
    const campaigns = await getCampaigns(db);
    for (const campaign of campaigns) {
      try {
        const puzzleSolutions = await db.collection('puzzlesolutions').find({ campaignToken: campaign.campaignToken }).toArray();

        const participants = [];
        for (const puzzleSolution of puzzleSolutions) {
          let decrypted = JSON.parse(cryptoUtils.decryptData(puzzleSolution.data));
          Object.assign(decrypted, { createdAt: puzzleSolution.createdAt.toLocaleString()});
          participants.push(decrypted);
        }

        if (participants.length > 0) {
          const workbook = export2excel(participants);
          const buf = await workbook.commit();

          const customer = await db.collection('customers').findOne({ _id: campaign.customer_id });
          const mailData = {
            reportDate: moment().locale('de').format('dddd D. MMMM YYYY'),
            reportSalutation: customer.salutation,
            reportFirstname: customer.firstname,
            reportLastname: customer.lastname
          };
          const attachment = [{
            filename: 'report-' + moment().format('YYYY-MM-DD') + '.xlsx',
            content: buf.stream
          }];
          await mailer.send(customer.emailAddress, 'reportCompetitionParticipants', mailData, attachment);
        }
      } catch (err) {
        console.log(err);
      }
    }
  } catch (err) {
    console.log(err);
  }
}

async function _sendAdminReport(campaignToken) {
  try {
    const db = await connection.connectDB();
    const puzzleSolutions = await db.collection('puzzlesolutions').find({campaignToken}).toArray();
    const participants = [];
    for (const puzzleSolution of puzzleSolutions) {
      let decrypted = JSON.parse(cryptoUtils.decryptData(puzzleSolution.data));
      Object.assign(decrypted, {createdAt: puzzleSolution.createdAt.toLocaleString()});
      participants.push(decrypted);
    }
    if (participants.length > 0) {
      const workbook = export2excel(participants);
      const buf = await workbook.commit();

      const mailData = {
        reportDate: moment().locale('de').format('dddd D. MMMM YYYY'),
        reportSalutation: 'Herr',
        reportFirstname: 'Admin',
        reportLastname: 'Flyer-Experten'
      };
      const attachment = [{
        filename: 'report-' + moment().format('YYYY-MM-DD') + '.xlsx',
        content: buf.stream
      }];
      await mailer.send('niklas@flyer-experten.com', 'reportCompetitionParticipants', mailData, attachment);
    }
  } catch (err) {
    console.log(err);
  }
}

function startScheduler() {
  new CronJob('0 0 3 * * *', async function() {
    try {
      await exportCampaigns()
    } catch (err) { console.error(err); }
  }, null, true);
}

module.exports = {
  startScheduler,
  exportCampaigns,
  _sendAdminReport
};