const MongoClient = require('mongodb').MongoClient;

let db;
exports.connectDB = function() {
  return new Promise((resolve, reject) => {
    if (db) return resolve(db);

    // Use connect method to connect to the Server
    MongoClient.connect(process.env.MONGODB_ADDON_URI, (err, _db) => {
      if (err) return reject(err);
      db = _db;
      resolve(_db);
    });
  });
};