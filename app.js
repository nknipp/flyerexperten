const express = require('express');
const path = require('path');
const logger = require('morgan');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');

const puzzleSolution = require('./routes/puzzlesolution');
const solvePuzzle = require('./routes/solvepuzzle');
const requestOffer = require('./routes/requestoffer');
const contactRequest = require('./routes/contactrequest');
const jobApplication = require('./routes/jobapplication');
const conceptionRequest = require('./routes/conceptionrequest');

require('./components/campaignExporter').startScheduler();

const app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/puzzlesolution', puzzleSolution);
app.use('/solvepuzzle', solvePuzzle);
app.use('/requestoffer', requestOffer);
app.use('/contactrequest', contactRequest);
app.use('/jobapplication', jobApplication);
app.use('/conceptionrequest', conceptionRequest);

app.get('/sendAdminReport', async (req, res, next) => {
  await require('./components/campaignExporter')._sendAdminReport(req.query.ct);
  res.status(200).end();
});

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  const err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
