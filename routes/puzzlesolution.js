const express = require('express');
const router = express.Router();

const conn = require('../components/databaseConnection');

async function renderSolution(req, res) {
  const campaignToken = req.query.ct;
  let formData = Object.assign({}, { 'campaigntoken': campaignToken });
  try {
    const db = await conn.connectDB();
    const campaign = await db.collection('campaigns').findOne({ campaignToken });
    Object.assign(formData, { campaign })
  } catch (err) {
    console.error(err);
  }
  const result = { errors: undefined, data: formData };
  res.render('puzzlesolution', result);
}

router.get('/', function(req, res, next) {
  renderSolution(req, res);
});

router.post('/', function(req, res, next) {
  renderSolution(req, res);
});

module.exports = router;
