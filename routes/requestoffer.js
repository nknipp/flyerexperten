const url = require('url');
const util = require('util');
const express = require('express');
const validate = require('validate.js');
const router = express.Router();

const cryptoUtils = require('../utils/crypto');
const mailer = require('../utils/mailer');
const conn = require('../components/databaseConnection');

const constraints = {
  offerAsOptions: {
    presence: { message: 'Bitte wählen Sie Bestellen als aus.' }
  },
  offerFirstname: function(value, attributes, attributeName, options, constraints) {
    if (attributes.offerAsOptions !== 'private') return null;
    return {
      presence: {message: 'Bitte geben Sie Ihren Vornamen ein.'}
    };
  },
  offerLastname:  function(value, attributes, attributeName, options, constraints) {
    if (attributes.offerAsOptions !== 'private') return null;
    return {
      presence: {message: 'Bitte geben Sie Ihren Nachnamen ein.'}
    };
  },
  offerCompanyName:  function(value, attributes, attributeName, options, constraints) {
    if (attributes.offerAsOptions !== 'company') return null;
    return {
      presence: {message: 'Bitte geben Sie die Firma ein.'}
    };
  },
  offerPersonContact:  function(value, attributes, attributeName, options, constraints) {
    if (attributes.offerAsOptions !== 'company') return null;
    return {
      presence: {message: 'Bitte geben Sie den Namen der Kontaktperson ein.'}
    };
  },
  offerStreet: {
    presence: { message: 'Bitte geben Sie die Straße ein.' }
  },
  offerPostcode: {
    presence: { message: 'Bitte geben Sie die Postleitzahl ein.' },
    format: {
      pattern: /\d{5}/,
      message: 'Bitte geben Sie eine gültige (fünfstellige) Postleitzahl ein.'
    }
  },
  offerCity: {
    presence: { message: 'Bitte geben Sie den Ort ein.' }
  },
  offerPhone: {
    presence: { message: 'Bitte geben Sie die Telefonnummer ein.' }
  },
  offerEmail: {
    email: { message: 'Bitte geben Sie eine gültige E-Mail Adresse ein.' }
  }
};

router.get('/', async function(req, res, next) {
  const qs = url.parse(req.url, true);
  const offerObj = qs.query;
  const validationResult = validate(offerObj, constraints, { format: 'flat', fullMessages: false });
  if (validationResult === undefined) {
    try {
      const encryptedOffer = cryptoUtils.encryptData(offerObj);
      const enrichedOffer = Object.assign({}, { type: 'OfferRequest', createdAt: new Date() }, { data: encryptedOffer });
      const db = await conn.connectDB();
      db.collection('requests').insertOne(enrichedOffer);
      mailer.send(offerObj.offerEmail, 'requestoffer', offerObj);
      mailer.send(process.env.SMTP_RECEIVER, 'requestofferinternal', offerObj);
    } catch(err) {
      console.error(err);
    }
  }
  const result = Object.assign({}, { errors: validationResult });
  res.header('Access-Control-Allow-Origin', '*');
  res.json(result);
});

module.exports = router;
