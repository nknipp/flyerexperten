const url = require('url');
const express = require('express');
const validate = require('validate.js');
const crypto = require('crypto');
const router = express.Router();

const cryptoUtils = require('../utils/crypto');
const conn = require('../components/databaseConnection');

const constraints = {
  solution: {
    presence: { message: 'Bitte beantworten Sie die Gewinnspielfragen.' }
  },
  firstname: {
    presence: { message: 'Bitte geben Sie Ihren Vornamen ein.' }
  },
  lastname: {
    presence: { message: 'Bitte geben Sie Ihren Nachnamen ein.' }
  },
  street: {
    presence: { message: 'Bitte geben Sie die Straße ein.' }
  },
  postcode: {
    presence: { message: 'Bitte geben Sie eine Postleitzahl ein.' },
    format: {
      pattern: /\d{5}/,
      message: 'Bitte geben Sie eine gültige (fünfstellige) Postleitzahl ein.'
    }
  },
  city: {
    presence: { message: 'Bitte geben Sie einen Ort ein.' }
  },
  email: {
    email: { message: 'Bitte geben Sie eine gültige E-Mail Adresse ein.' }
  },
  fullAgeParentalConsent: function(value, attributes, attributeName, options, constraints) {
    return value !== attributeName ?
      { presence: { message: 'Sie müssen bestätigen, dass Sie bereits 18 Jahre alt sind oder die Zustimmung Ihrer Eltern haben.' }} :
      undefined;
  },
  acceptTermsAndConditions: function(value, attributes, attributeName, options, constraints) {
    return value !== attributeName ?
      { presence: { message: 'Sie müssen die Teilnahmebedingungen akzeptieren.' }} :
      undefined;
  }
};

function buildHashedSolutionKey(solutionObj) {
  const solutionKey = solutionObj.campaigntoken + '-' +
    solutionObj.firstname.replace(/[ -]/g, '') + '-' +
    solutionObj.lastname.replace(/[ -]/g, '') + '-' +
    solutionObj.street.replace(/[ -]/g, '') + '-' +
    solutionObj.postcode + '-' +
    solutionObj.city.replace(/[ -]/g, '');

  const hash = crypto.createHmac('sha512', process.env.ENCRYPTION_PASS);
  hash.update(solutionKey.toLowerCase());
  return hash.digest('hex');
}

function removeUnnecesarryDataFromSolutionObj(solutionObj) {
  const tmpSolutionObj = Object.assign({}, solutionObj);
  delete tmpSolutionObj.campaigntoken;
  delete tmpSolutionObj.fullAgeParentalConsent;
  delete tmpSolutionObj.acceptTermsAndConditions;
  for (let idx = 0; ; idx += 1) {
    if (tmpSolutionObj['question-' + idx] === undefined) break;
    delete tmpSolutionObj['question-' + idx];
  }
  return tmpSolutionObj;
}

function enrichSolutionObj(solutionObj, solutionKey, campaignToken) {
  return Object.assign({}, { key: solutionKey, campaignToken: campaignToken, createdAt: new Date() }, { data: solutionObj });
}

router.post('/', async function(req, res, next) {
  const solutionObj = req.body;
  const validationResult = validate(solutionObj, constraints, { format: 'flat', fullMessages: false });
  if (validationResult === undefined) {
    try {
      const hashedSolutionKey = buildHashedSolutionKey(solutionObj);
      const cleanedSolutionObj = removeUnnecesarryDataFromSolutionObj(solutionObj);
      const encryptedSolutionObj = cryptoUtils.encryptData(cleanedSolutionObj);
      const enrichedSolutionObj = enrichSolutionObj(encryptedSolutionObj, hashedSolutionKey, req.body.campaigntoken);
      const db = await conn.connectDB();
      db.collection('puzzlesolutions').updateOne({ key: hashedSolutionKey }, enrichedSolutionObj, { upsert: true });
      res.json({});
    } catch(err) {
      console.error(err);
    }
  } else {
    const result = Object.assign({}, { errors: validationResult });
    res.json(result);
  }
});

module.exports = router;
