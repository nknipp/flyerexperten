const url = require('url');
const util = require('util');
const express = require('express');
const validate = require('validate.js');
const router = express.Router();

const cryptoUtils = require('../utils/crypto');
const mailer = require('../utils/mailer');
const conn = require('../components/databaseConnection');

const constraints = {
  contactname: {
    presence: { message: 'Bitte geben Sie Ihren Namen ein.' }
  },
  contactemail: {
    email: { message: 'Bitte geben Sie eine gültige E-Mail Adresse ein.' }
  },
  contactsubject: {
    presence: { message: 'Bitte geben Sie einen Betreff ein.' }
  },
  contactmessage: {
    presence: { message: 'Bitte geben Sie eine Nachricht ein.'}
  }
};

router.get('/', async function(req, res, next) {
  const qs = url.parse(req.url, true);
  const contactObj = qs.query;
  const validationResult = validate(contactObj, constraints, { format: 'flat', fullMessages: false });
  if (validationResult === undefined) {
    try {
      const encryptedContact = cryptoUtils.encryptData(contactObj);
      const enrichedContact = Object.assign({}, { type: 'ContactRequest',  createdAt: new Date() }, { data: encryptedContact });
      const db = await conn.connectDB();
      db.collection('requests').insertOne(enrichedContact);
      mailer.send(process.env.SMTP_RECEIVER, 'contactrequestinternal', contactObj);
    } catch(err) {
      console.error(err);
    }
  }
  const result = Object.assign({}, { errors: validationResult });
  res.header('Access-Control-Allow-Origin', '*');
  res.json(result);
});

module.exports = router;
