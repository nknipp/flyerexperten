const url = require('url');
const util = require('util');
const express = require('express');
const validate = require('validate.js');
const router = express.Router();

const mailer = require('../utils/mailer');
const conn = require('../components/databaseConnection');

const constraints = {
  firstname: {
    presence: { message: 'Bitte geben Sie Ihren Vornamen ein.' }
  },
  lastname: {
    presence: { message: 'Bitte geben Sie Ihren Nachnamen ein.' }
  },
  birthdate: {
    presence: { message: 'Bitte geben Sie Ihr Geburtsdatum ein.' }
  },
  email: {
    email: { message: 'Bitte geben Sie eine gültige E-Mail Adresse ein.' }
  },
  street: {
    presence: { message: 'Bitte geben Sie Ihre Straße ein.'}
  },
  postcode: {
    presence: { message: 'Bitte geben Sie Ihre Postleitzahl ein.'}
  },
  city: {
    presence: { message: 'Bitte geben Sie Ihren Ort ein.'}
  },
  availablefrom:  function(value, attributes, attributeName, options, constraints) {
    if (attributes.available === 'immer') return null;
    return {
      presence: {message: "Bitte geben Sie ein Von-Verfügbarkeitsdatum an"}
    };
  },
  availableto:  function(value, attributes, attributeName, options, constraints) {
    if (attributes.available === 'immer') return null;
    return {
      presence: {message: "Bitte geben Sie ein Bis-Verfügbarkeitsdatum an"}
    };
  },
  postcoderegion: {
    presence: { message: 'Bitte geben Sie ein oder mehrere PLZ-Gebiete ein.'}
  },
  text: {
    presence: { message: 'Bitte geben Sie eine Nachricht ein.'}
  }
};

router.get('/', async function(req, res, next) {
  const qs = url.parse(req.url, true);
  const jobObj = qs.query;
  const validationResult = validate(jobObj, constraints, { format: 'flat', fullMessages: false });
  if (validationResult === undefined) {
    try {
      const enrichedJob = Object.assign({}, { createdAt: new Date() }, jobObj);
      const db = await conn.connectDB();
      await db.collection('flyerdeliverer').insertOne(enrichedJob);
      await mailer.send(process.env.SMTP_JOBAPPLCATION_RECIPIENT, 'jobapplicationinternal', { data: jobObj });
      await mailer.send(jobObj.email, 'jobapplication', { data: jobObj });
    } catch(err) {
      console.error(err);
    }
  }
  const result = Object.assign({}, { errors: validationResult });
  res.header('Access-Control-Allow-Origin', '*');
  res.json(result);
});

module.exports = router;
