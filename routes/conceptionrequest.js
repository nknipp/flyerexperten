const url = require('url');
const util = require('util');
const express = require('express');
const validate = require('validate.js');
const router = express.Router();

const cryptoUtils = require('../utils/crypto');
const mailer = require('../utils/mailer');
const conn = require('../components/databaseConnection');

const constraints = {
  firstname: function(value, attributes, attributeName, options, constraints) {
    if (attributes.conceptionAs !== 'private') return null;
    return {
      presence: {message: 'Bitte geben Sie Ihren Vornamen ein.'}
    };
  },
  lastname: function(value, attributes, attributeName, options, constraints) {
    if (attributes.conceptionAs !== 'private') return null;
    return {
      presence: {message: 'Bitte geben Sie Ihren Nachnamen ein.'}
    };
  },
  companyName: function(value, attributes, attributeName, options, constraints) {
    if (attributes.conceptionAs !== 'company') return null;
    return {
      presence: {message: 'Bitte geben Sie die Firma ein.'}
    };
  },
  personContact:  function(value, attributes, attributeName, options, constraints) {
    if (attributes.conceptionAs !== 'company') return null;
    return {
      presence: {message: 'Bitte geben Sie den Namen der Kontaktperson ein.'}
    };
  },
  street: {
    presence: { message: 'Bitte geben Sie die Straße ein.' }
  },
  postcode: {
    presence: { message: 'Bitte geben Sie die Postleitzahl ein.' },
    format: {
      pattern: /\d{5}/,
      message: 'Bitte geben Sie eine gültige (fünfstellige) Postleitzahl ein.'
    }
  },
  city: {
    presence: { message: 'Bitte geben Sie den Ort ein.' }
  },
  phone: {
    presence: { message: 'Bitte geben Sie die Telefonnummer ein.' }
  },
  email: {
    email: { message: 'Bitte geben Sie eine gültige E-Mail Adresse ein.' }
  },
  industry: {
    presence: { message: 'Bitte geben Sie die Telefonnummer ein.' }
  }
};

router.get('/', async function(req, res, next) {
  const qs = url.parse(req.url, true);
  const conceptionObj = qs.query;
  const validationResult = validate(conceptionObj, constraints, { format: 'flat', fullMessages: false });
  if (validationResult === undefined) {
    try {
      const encryptedConception = cryptoUtils.encryptData(conceptionObj);
      const enrichedConception = Object.assign({}, { type: 'ConceptionRequest', createdAt: new Date() }, { data: encryptedConception });
      const db = await conn.connectDB();
      db.collection('requests').insertOne(enrichedConception);
      mailer.send(conceptionObj.email, 'conceptionrequest', { data: conceptionObj });
      mailer.send(process.env.SMTP_RECEIVER, 'conceptionrequestinternal', { data: conceptionObj });
    } catch(err) {
      console.error(err);
    }
  }
  const result = Object.assign({}, { errors: validationResult });
  res.header('Access-Control-Allow-Origin', '*');
  res.json(result);
});

module.exports = router;
