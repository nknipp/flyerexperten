const fs = require('fs');
const path = require('path');
const nodemailer = require('nodemailer');
const ejs = require('ejs');

const mailTemplatesDir = path.join(__dirname, '../mailTemplates/');

function loadTemplateByName(templateName) {
  const templateNameWithExtension = templateName.endsWith('.ejs') ? templateName : templateName + '.ejs';
  return new Promise((resolve, reject) => {
    fs.readFile(path.join(mailTemplatesDir, templateNameWithExtension), 'utf8', (err, data) => {
      if (err) return reject(err);
      resolve(data);
    });
  });
}

function getRenderedTemplateData(template, data) {
  const renderedTemplate = ejs.render(template, data);
  const mailSubject = /<mailsubject>([\s\S]+)<\/mailsubject>/.exec(renderedTemplate)[1];
  const mailBody = /<mailbody>([\s\S]+)<\/mailbody>/.exec(renderedTemplate)[1];
  const mailPlain = /<mailplain>([\s\S]+)<\/mailplain>/.exec(renderedTemplate)[1];
  return {mailSubject, mailBody, mailPlain};
}

function getTransport() {
  return nodemailer.createTransport(JSON.parse(process.env.SMTP_HOST));
}

function sendMail(transport, message) {
  return new Promise((resolve, reject) => {
    transport.sendMail(message, (err, info) => {
      if (err) return reject(err);
      resolve(info);
    })
  });
}

async function send(recipient, templateName, mailData, attachments = []) {
  try {
    const template = await loadTemplateByName(templateName);
    const {mailSubject, mailBody, mailPlain} = getRenderedTemplateData(template, mailData);

    const transport = getTransport();
    const message = {
      from: process.env.SMTP_SENDER,
      to: recipient,
      bcc: 'niklas@flyer-experten.com', //HACK Nur solange bis sichergestellt ist, dass es funktioniert.
      subject: mailSubject,
      text: mailPlain,
      html: mailBody,
      attachments: attachments
    };
    await sendMail(transport, message);
  } catch(e) {
    console.error(e);
  }
}

module.exports = {
  send
};