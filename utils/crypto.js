const crypto = require('crypto');

function encryptData(data) {
  const cipher = crypto.createCipher('aes-256-ctr', process.env.ENCRYPTION_PASS);
  let crypted = cipher.update(JSON.stringify(data), 'utf8', 'hex');
  crypted += cipher.final('hex');
  return crypted;
}

function decryptData(data) {
  const decipher = crypto.createDecipher('aes-256-ctr', process.env.ENCRYPTION_PASS);
  let decrypted = decipher.update(data, 'hex', 'utf8');
  decrypted += decipher.final('utf8');
  return decrypted;
}

module.exports = {
  encryptData,
  decryptData
};