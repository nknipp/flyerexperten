/**
 * Die Tests funktionieren nur, wenn in einem Terminal ein Mailserver (maildev) auf SMTP-Port 25000 läuft.
 */

const { MongoClient, ObjectId } = require('mongodb');
const campaignExporter = require('../components/campaignExporter');

process.env.MONGODB_ADDON_URI = 'mongodb://localhost/testCampaignExport';
process.env.ENCRYPTION_PASS = 'abcdefghijklmnopqrstuvwxyz';

process.env.SMTP_HOST = '{ "host": "127.0.0.1", "port": 25000, "secure": false, "ignoreTLS": true }';
process.env.SMTP_SENDER = 'noreply@flyer-experten.com';
process.env.SMTP_RECEIVER = 'info@flyer-experten.com';

let db = undefined;

async function initializeMongoDB() {
  db = await MongoClient.connect(process.env.MONGODB_ADDON_URI);
  await db.collection('customers').insertMany([{
    "_id" : ObjectId("59313efd933efcd7bfb1ac71"),
    "name" : "Kundy Fahrschule",
    "emailAddress" : "kundy-fahrschule@flyer-experten.com",
    "salutation" : "Herr",
    "firstname" : "Dieter",
    "lastname" : "Kundy"
  },{
    "_id" : ObjectId("5934fe262db4c0d33797e8aa"),
    "name" : "Express Fahrschule",
    "emailAddress" : "express-fahrschule@flyer-experten.com",
    "salutation" : "Herr",
    "firstname" : "Franz",
    "lastname" : "Mann"
  }]);
  await db.collection('campaigns').insertMany([{
    "customer_id" : ObjectId("59313efd933efcd7bfb1ac71"),
    "campaignToken" : "kundy201705",
    "createdAt" : new Date("2017-05-30T09:12:23.028Z"),
    "startDate" : new Date("2017-06-04T00:00:00.000Z"),
    "closingDate" : new Date("2017-06-07T00:00:00.000Z"),
    "appConfig" : {}
  },{
    "customer_id" : ObjectId("5934fe262db4c0d33797e8aa"),
    "campaignToken" : "express201704",
    "createdAt" : new Date("2017-03-30T09:12:23.028Z"),
    "startDate" : new Date("2017-06-06T00:00:00.000Z"),
    "closingDate" : new Date("2017-06-12T00:00:00.000Z"),
    "appConfig" : {}
  }]);
  await db.collection('puzzlesolutions').insertMany([{
    "key" : "5a82c2191890d9e3a7aea0ff8de42eb3a760df20b058e35f01e85cba2b28293262d4ed63d9fafd4787bac5d13c4b1d2ab428e28751e7ed70ae26bc31fd6294e6",
    "campaignToken" : "kundy201704",
    "createdAt" : new Date("2017-05-31T14:14:23.028Z"),
    "data" : "d927a5473c1809a1754d592ca4d84d237cfeee5b929f7d4e7e5aa6cbda61b850e68c68068bcc7ec8b2e3f47828686f50c23d2995ee3abfee3fa528bcc7cf24fec090e19acbbd7e04e90bab3dd3115fe18297c62a9f9e42f1f0e5106de548eb36a3fd25ab10678c2e9c8fad47cda20b5f8e6edc591baa3bfa8d431693010ba4d383a5c71a8acbd666b2837ef903eb5a12b63e040e81c12759a0b6aa4d87e681429cb0d3eb0e9c6e83f6680f8afa7355f9143a62c78b8f"
  },{
    "key" : "c988dd026be75fca390aa9c10c134477902a4064c8684da124b9ad617613e56c77582a4843df87d0c6659e61511fb7be7ce5b48a3e5e1b6c63e6259a50ed6923",
    "campaignToken" : "express201704",
    "createdAt" : new Date("2017-06-07T11:07:06.721Z"),
    "data" : "d927a5473c1809a1754d592ca4d84d237cfeee5b929c6d55305aefe93983ae56fcc96608cfc365c9b5f9fb74202f7748a7b0fc90f63ebeee3fa528bcc7cf24fec090e19acbbe7118ff1dec33dd4058e79597d77c878621c5e3a3546ced058d1fbefc22ff06738220ce90b140daae005ec976c44e1fa83ff89a4d18d20b16a9889bbda92b94cbd77ea69c74f948ac1155f77d080fc2926916ffd4a24781fd945cf2b9c0e45e92"
  }]);
}

beforeAll(() => {
  return initializeMongoDB();
});

afterAll(() => {
  return db.dropDatabase()
    .then(() => {
      db.close();
    });
});

test('no participants to export', () => {
  return campaignExporter.exportCampaigns();
});